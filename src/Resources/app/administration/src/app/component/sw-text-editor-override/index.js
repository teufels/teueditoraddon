/* global Shopware */

import template from './sw-text-editor.html.twig'

const { Component } = Shopware;

Component.override('sw-text-editor', {
    template,

    data() {
        return {
            mediaModalIsOpen: false
        }
    },
    created() {
        // add media button
        this.buttonConfig.push({
            title: this.$tc('sw-text-editor-toolbar.title.media'),
            icon: 'regular-image-xs',
            position: 'left',
            handler: () => {
                this.mediaModalIsOpen = true
            }
        });


    },
    methods: {
        onModalClosed(selection) {
            if (selection.length) {
                const media = selection[0]
                document.execCommand('insertHTML', false, '<img src="' + media.url + '" class="img-fluid cms-image" alt="' + media.alt + '" title="' + media.title +'" />')
            }
        },
        getContentValue() {
            if (!this.$refs.textEditor || !this.$refs.textEditor.innerHTML) {
                return null;
            }

            // do not return null if there are only images in this editor
            if (!this.$refs.textEditor.getElementsByTagName('img') &&
                (
                !this.$refs.textEditor.textContent ||
                !this.$refs.textEditor.textContent.length ||
                this.$refs.textEditor.textContent.length <= 0
                )
            ) {
                return null;
            }

            return this.$refs.textEditor.innerHTML;
        },
    }
});

