/* global Shopware */

const { Component } = Shopware;

Component.override('sw-text-editor-link-menu', {
    created() {
        this.buttonVariantList.push(
            {
                id: 'download',
                name: this.$tc('sw-text-editor-toolbar.link.buttonVariantDownload'),
            },
        );
    }
});